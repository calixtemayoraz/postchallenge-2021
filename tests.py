import unittest
from main import *


class TestingAll(unittest.TestCase):

    def test_distances(self):
        """sanity check to make sure I implemented this correctly."""
        graph = {
            "nodes": {
                "node1": [0, 0],
                "node2": [3, 4],
            },
            "edges": [
                ["node2", "node1"]
            ]
        }
        with self.subTest():
            self.assertEqual(calc_distance_between(graph, "node1", "node2"), 5)  # 3^2 + 4^2 = 5^2. Pythagoras!
