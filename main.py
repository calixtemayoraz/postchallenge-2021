import json
import numpy as np
import networkx as nx
from matplotlib import pyplot as plt


def load_json():
    """
    Loads the json graph and returns a dict object
    """
    with open("graph.json", "r") as f:
        out = json.load(f)
        f.close()
    return out


def calc_distance_between(graph_list: dict, node1: str, node2: str):
    """
    calculates and returns the distance between two nodes depending on their position in the graph file
    """

    # calculate the euclidean distance between the two points.
    # first, find their coordinates and make them numpy arrays
    node1 = np.array(graph_list['nodes'][node1])
    node2 = np.array(graph_list['nodes'][node2])

    # return the distance using the norm of the vector they form.
    return np.linalg.norm(node1 - node2)


def generate_graph(graph_list: dict):
    """
    Prepares the graph from the parsed graph file
    """
    graph = nx.Graph()
    # add all the nodes
    graph.add_nodes_from(graph_list['nodes'].keys())
    # now add the edges
    for edge in graph_list['edges']:
        weight = calc_distance_between(graph_list, edge[0], edge[1])
        graph.add_edge(edge[0], edge[1], weight=weight)

    return graph


def calc_shortest_path():
    """
    loads the graph file, and calculates the shortest path, with total distance
    """
    graph_list = load_json()
    graph = generate_graph(graph_list)
    # let networkx do the heavy lifting by calculating the shortes path with Dijkstra
    path = nx.algorithms.shortest_path(graph, source="PostFinanceHQ", target="CharityEvent", weight="weight")
    length = nx.algorithms.shortest_path_length(graph, source="PostFinanceHQ", target="CharityEvent", weight="weight")
    # tada! print the path
    print("Shortest path:", path)
    # print the individual distances between path nodes
    for i, node in enumerate(path):
        if node == "CharityEvent":
            break
        print(graph.edges[node, path[i+1]])

    # print total distances
    print(f"total length: {length}")
    # plot the graph
    nx.draw_kamada_kawai(graph, with_labels=True)
    plt.show()


if __name__ == '__main__':
    calc_shortest_path()