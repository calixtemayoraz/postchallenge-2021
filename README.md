# SwissPost IT challenge 2021
Here is my solution to this problem, which is a straightforward shortest path problem.

## Installation
clone the repository
```shell
git clone https://gitlab.com/calixtemayoraz/postchallenge-2021
```
create a virtual environment, and source into it
```shell
virtualenv env 
source env/bin/activate
```
download the required packages
```shell
pip install -r requirements.txt
```
run!
```
python main.py
```

## Thoughts
This was quite a simple challenge if you knew of the tools to efficiently calculate the desired result.
The real tedious part of this challenge was to set up the graph, by copying down all the coordinates of the nodes
and the edges between nodes.

Here is the challenge (for the future when it is taken down.)
![](img/img1.png)
![](img/img2.png)
![](img/img3.png)
![](img/img4.png)